// const cron = require("node-cron");
const express = require("express");
const axios = require("axios");
const Path = require('path')
const fs = require("fs");
const ics = require('ics')
const moment = require('moment-timezone');

app = express();


app.listen(3128);

const folder = 'events/images/'
const folderAttendee = 'attendees/images/'
const folderICS = 'events/ics/'
if (!fs.existsSync(`./${folder}`)) {
    fs.mkdirSync(folder, { recursive: true })
}
if (!fs.existsSync(`./${folderAttendee}`)) {
    fs.mkdirSync(folderAttendee, { recursive: true })
}
if (!fs.existsSync(`./${folderICS}`)) {
    fs.mkdirSync(folderICS, { recursive: true })
}

let html = `
<button type="button" onclick="proceed();">do</button> 
<script>
function proceed () {
    var form = document.createElement('form');
    form.setAttribute('method', 'post');
    form.setAttribute('action', '/');
    form.style.display = 'hidden';
    document.body.appendChild(form)
    form.submit();
}
</script>
`


const server = `https://openpublishingfest2.cloud68.co`;
let token;



async function login(username, password) {
    axios
        .post(`${server}/auth/local`, {
            identifier: username,
            password: password,
        })
        .then(function (response) {

            token = response.data.jwt;
            // document.querySelector(".form").remove();
            if (response) {
                load()
                loadAttendees()
            }
        })
        .catch(function (error) {
            console.log('there is an error:', error)
            // p.textContent = "sorry mate can’t connect, forgot you pass again?";
        })
        .finally(function () { });
}



async function load() {
    axios
        .get(`${server}/events?_limit=-1`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        .then(function (response) {
            const data = response.data;
            backupDB(`events/data.js`, data);
            backupIMG(data);
            createICS(data);
            // console.log('response', response)
        })
        .catch(function (error) {
            console.log('error:', error);
        })
}
async function loadAttendees() {
    axios
        .get(`${server}/participants?_limit=-1`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        .then(function (response) {
            const data = response.data;
            backupDBAttendees(`attendees/data.js`, data);
            backupIMGAttendees(data);
            // console.log('response', response)
        })
        .catch(function (e) {
            console.log('e:', e);
        })
}

async function backupDB(filename, data) {
    let content = JSON.stringify(data);
    fs.writeFileSync(filename, `const allEventsData =` + content), 'utf-8', function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    };
}
async function backupIMG(data) {
    // for (var key in data) {
    var keys = Object.keys(data);
    for (var i = 0; i < keys.length; i++) {
        if (data[keys[i]].hasOwnProperty('url')) {
            data[keys[i]].images.forEach(img => {
                // if (img.ext != '.bin') {
                downloadImage(server + img.url, img.hash + img.ext, 'events/images');

                // }
            });
        }
    }
}


async function backupDBAttendees(filename, data) {
    let content = JSON.stringify(data);
    fs.writeFileSync(filename, `const allAttendeesData =` + content), 'utf-8', function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    };
}
async function backupIMGAttendees(data) {
    // for (var key in data) {
    var keys = Object.keys(data);
    for (var i = 0; i < keys.length; i++) {
        if (data[keys[i]].picture === null) {

        } else if (data[keys[i]].picture.ext != '.bin' || data[keys[i]].picture != null) {

            downloadImage(server + data[keys[i]].picture.url, data[keys[i]].picture.hash + data[keys[i]].picture.ext, 'attendees/images');

        }
    };
}

async function createICS(data) {
    data.forEach(ev => {
        const dateStart = moment
            .tz(`${ev.date}T${ev.time}.000`, ev.timezone).utc();


        // parse with NYC time, and then convert to UTC
        // let startTime = moment.tz(strStartTime, "America/New_York").utc();
        // let endTime = moment.tz(strEndTime, "America/New_York").utc();

        // convert dates to array
        startTime = [dateStart.get('year'), dateStart.get('month') + 1, dateStart.get('date'), dateStart.get('hour'), dateStart.get('minute')];

        ics.createEvent({
            title: ev.title,
            description: ev.description + `  \n\nWARNING: TIME AND DATES MAY CHANGE, \nBE SURE TO CHECK THE EVENT ON THE WEBSITE: \n\nhttps://www.openpublishingfest.org/\n\n and get the latest news on https://www.openpublishingfest.org/#event-${ev.id}`,
            busyStatus: 'FREE',
            start: startTime,
            URL: ev.link,
            startInputType: 'utc', // I define the start time as UTC
            duration: { minutes: 60 }
            // start: [2000, 05,16,10, 30],
        }, (error, value) => {
            if (error) {
                console.log(error)
            }

            fs.writeFileSync(`${__dirname}/${folderICS}/event-${ev.id}.ics`, value)
            console.log('ics created:', ev.title)
        })
    })
}



app.use(express.static('events'))

app.get('/', function (req, res) {
    res.send(html);
});

app.post('/', function (req, res) {
    login(process.env.CALUSERNAME, process.env.CALPASSWORD);
    res.send('done');
});

async function downloadImage(url, file, location) {

    const path = Path.resolve(__dirname, location, file)
    const writer = fs.createWriteStream(path)

    console.log('image copied:', url)

    const response = await axios({
        url,
        method: 'GET',
        responseType: 'stream'
    })

    response.data.pipe(writer)

    return new Promise((resolve, reject) => {
        writer.on('finish', resolve)
        writer.on('error', reject)
    })
}

