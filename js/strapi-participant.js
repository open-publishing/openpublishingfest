// set up form
const formElement = document.querySelector('#form');
console.log('formElement', formElement)



formElement.addEventListener("submit", e => {


    e.preventDefault();

    

    // list of events
    const eventsOk = document.createElement("input");
    eventsOk.setAttribute("name", "events");
    eventsOk.value = select.selected().join(",");
    formElement.insertAdjacentElement("beforeend",eventsOk);
    
    const formElements = formElement.elements;
    console.log('formElements', formElements);
    // enrico
    if (formElements[8].value == "") {
        const formContent = new FormData(formElement);

        const data = {};

        for (let i = 0; i < formElements.length; i++) {

            const currentElement = formElements[i];

            if (!['submit', 'file'].includes(currentElement.type) && currentElement.name != "enrico") {
                // formContent.append(currentElement.name, currentElement.value)
                if (currentElement.type == "radio" && currentElement.checked) {
                    currentElement.value = currentElement.value.replace(`"`, ``);
                    currentElement.value = new Boolean(currentElement.value);
                }
                // formContent.append("shareUserName",)


                data[currentElement.name] = currentElement.value;
            } else if (currentElement.type === 'file' && currentElement.length > 0) {
                for (let i = 0; i < currentElement.files.length; i++) {
                    const file = currentElement.files[i];
                    // console.log("file", file)

                    // formContent.append(currentElement.name, file, File.name);
                    // data[currentElement.name] = currentElement.value;

                }
            }
            // else if (currentElement.name === "eventsPre") {
            //     const eventsOk = document.createElement("input");
            //     eventsOk.setAttribute("name", "events");
            //     eventsOk.value = select.selected().join(",");
                

            //     currentElement.remove();
            //     // currentElement.value = select.selected().join(",");
                
            // }

        }
        const stringdata = data;
        console.log('"data", JSON.stringify(stringdata)', JSON.stringify(stringdata));
        formContent.append("data", JSON.stringify(data));
        
        formContent.delete("enrico");


        axios
            .post(`https://openpublishingfest2.cloud68.co/participants`, formContent, {
                onUploadProgress: event => {
                    console.log(event.loaded, event.total);
                    document.querySelector("section.update").innerHTML = `<p class="event-loading">${Math.floor(event.loaded / event.total * 100)}%</p>`
                    if (event.loaded == event.total) {
                        document.querySelector("section.update").innerHTML = "Please wait while we’re uploading your submission to our servers."
                    }
                }
            })
            .then(function (response) {
                console.log(response);
                document.querySelector("section.update").innerHTML = '<p class="thanks">Welcome to the band! We’ll add your name to the list.</p>';
            })
            .catch(error => {
                console.log(error);
                console.log('...formContent', ...formContent)
                document.querySelector("section.update").innerHTML = `
                <p class="thanks">An error occured:  ${error}</p>
                <p class="thanks">There has been an error while uploading your form, please contact <a href="mailto:julien@coko.foundation">julien @ coko . foundation</a>. or <a href="participant.html">try again</a>.</p>`;
                scrollTo(document.querySelector("section.update"));
            });

    }

});

function sizeOfFile() {

    var filesize = document.querySelector('input[type="file"]').files[0].size;
    if (filesize > 2000000) {
        this.value = "";
        alert("images must be 2mb maximum");
    }

    console.log(filesize);
}

// function mailValidation() {


// }

function emailIsValid(email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
}

function checkMail(mail) {
    if (!emailIsValid(mail)) {
        alert('There is a problem with your email adress. It should look like this: awesome@open.fest')
    }
}


const emailZone = document.querySelector('input[type="email"]');
//   document.querySelector('input[type="email"]').addEventListener('change', emailIsValid(document.querySelector('input[type="email"]').value));

// emailZone.addEventListener("change", console.log('change'));


const data = allEventsData;

data.sort(function (a, b) {
    return compareStrings(a.title, b.title);
})
const liste = document.querySelector('#chosenEvents');
const acceptedliste = document.querySelector('#chosenEvents');

// populate the events selector
var keys = Object.keys(data);
let last = "";
async function generateData() {
for (var i = 0; i < keys.length; i++) {
    const event = data[keys[i]]


    if (event.hasOwnProperty('title')) {
        // console.log(data[keys[i]].title);
        // console.log(data[keys[i]].id);
        if (event.title != last && event.accept === true) {
            const option = document.createElement("option");
            option.value = `${event.id}`;
            option.textContent = event.title;
            // option.setAttribute("onclick", "checkEvent(this)")
            liste.insertAdjacentElement("beforeend", option)
        }

    }
    last = event.title

}
}

generateData();

function compareStrings(a, b) {
    // Assuming you want case-insensitive comparison
    a = a.toLowerCase();
    b = b.toLowerCase();

    return (a < b) ? -1 : (a > b) ? 1 : 0;
}
var select = new SlimSelect({
    select: '#chosenEvents'
});



function maxText(el) {
    if (el.getAttribute("maxlength")) {
    var max = el.getAttribute("maxlength");
    var len = el.value.length;
    var char = max - len;
    el.closest("label").querySelector(".counter").innerHTML = `${len}/${max}`;
    }
}

// document.querySelectorAll('textarea').forEach(text => {
//     text.addEventListener("input", maxText(text));
//     text.addEventListener('onkeyup', maxText(text));
// })



    var txts = document.getElementsByTagName('TEXTAREA'); 
  
    for(var i = 0, l = txts.length; i < l; i++) {
      if(/^[0-9]+$/.test(txts[i].getAttribute("maxlength"))) { 
        var func = function() { 
          var len = parseInt(this.getAttribute("maxlength"), 10); 
  
          if(this.value.length > len) { 
            alert('Maximum length exceeded: ' + len); 
            this.value = this.value.substr(0, len); 
            return false; 
          } 
          maxText(this);
          
        }
  
        txts[i].onkeyup = func;
        txts[i].onblur = func;
      } 
    };
  
  
  
  