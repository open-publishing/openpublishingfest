//  const server = `http://localhost:1337`;
// const server = `https://openpublishingfest2.cloud68.co`;
// const server = `test`;


var registration = `<svg class="registration-icon" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" stroke="var(--white);" fill="var(--colorItem)" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="bevel"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7.01" y2="7"></line></svg>`

let showAll;
let showEventToggle;
var md = window.markdownit();

const getNavigatorLanguage = () =>
  navigator.languages && navigator.languages.length
    ? navigator.languages[0]
    : navigator.userLanguage ||
    navigator.language ||
    navigator.browserLanguage ||
    "en"; 

// console.log(getNavigatorLanguage)
const dayStart = 17, dayEnd = 30;

let container = document.querySelector("#myData");
let containerList = document.querySelector("#list");

// let localDate = Intl.DateTimeFormat().resolvedOptions().timeZone;
let localDate = moment.tz.guess();
moment.locale(getNavigatorLanguage());


// moment.locale('en');

// let localDate = 'UTC';

// to build the static
// moment.locale("en");
// localDate = "UTC";


const selector = document.querySelector("#timezoneSelect");


let targetDate = localDate;

selector.querySelectorAll("option").forEach((zn) => {
  if (zn.value == targetDate) {
    zn.setAttribute("selected", "selected");
  }
});


async function loadNoDb() {

  listAllTent();
  const data = allEventsData;

  data.forEach(el => {
    el.fullTime = moment(`${el.date}T${el.time}`).tz(el.timezone).tz(targetDate);
  });
  // willLoad();
  sortedDates = orderByDate(data);
  createCalendar(sortedDates, targetDate);
  createList(sortedDates, targetDate);
  hasLoaded();
  openURL();
}
loadNoDb();

function createCalendar(sortedDates, targetDate) {
  container.innerHTML = "";

  for (let dayCount = dayStart; dayCount < dayEnd + 1; dayCount++) {

    const day = document.createElement("section");

    const dayList = document.createElement("ul");
    dayList.classList.add("day");
    dayList.id = `day-${dayCount}`;

    let dayNumber = moment(`05/${dayCount}/2020`);

    dayList.classList.add(dayNumber.format("dddd"));
    for (let i = 0; i < sortedDates.length; i++) {
      const element = sortedDates[i];
      let color, tentName;

      switch (element.tent) {
        case 'demo':
          color = "sunny";
          hash="demo"
          tentName = 'Demo & How To'
          break;
        case 'performance':
          color = "symbols"
          hash='performance'
          tentName = 'Performance'
          break;
        case 'firesideChats':
          color = "waters"
          tentName = 'Fireside Chats'
          hash = 'fireside-chats'
          break;
        case 'discussion':
          color = "forest"
          tentName = 'Discussion'
          hash = 'discussion'
          break;
        case 'micropublications':
          color = "spades"
          tentName = 'Micropublications'
          hash = 'micropubs'
          break;
        case 'longEvent':
          color = "mountains"
          tentName = 'work of Bob Stein'
          hash = 'micropubs'
          break;
        default:
          color = "black"
          tentName = 'this event hasn’t been curated yet'
          break
      }
      const dateEvent = moment
        .tz(`${element.date}T${element.time}.000`, element.timezone)
        .clone()
        .tz(targetDate);

      if (dateEvent.format("D") == dayCount) {
        const link = document.createElement("a");
        link.setAttribute("href", `#event-${element.id}`);
        link.classList.add(color);
        link.classList.add(`tent-${color}`);
        link.classList.add(`tent-${hash}`);
        const item = document.createElement("li");
        dateEvent;
        link.classList.add("event");
        link.setAttribute('onclick', `showEvent(this)`);
        // link.setAttribute("onclick", "alert('Clic sur le bouton');")
        if (element.accept === true) {
          link.classList.add("accepted");
        }
        if (element.registration === true) {
          link.classList.add("registration");

        }
        link.dataset.sourceTime = dateEvent.format("HH:mm");
        link.dataset.sourceDate = element.date;

        item.setAttribute("style", `--colorItem: var(--${color})`);
        item.innerHTML = `<p class="meta"><span class="local">${dateEvent.format(
          "LT (z)"
        )}</span>`;
        item.innerHTML += `<h2>${element.title} ${(element.registration === true) ? registration : ""}</h2>`;
 
        // tags / language
        if (element.tags && element.tags != null) {
          element.tags = element.tags.replace(/,\s*$/, ""); 
          const tagList = element.tags.split(", ");
          // tagList = 
          // console.log('brefoe', tagList);
          // console.log('post:' , tagList);
          element.tagIn = "";

          tagList.forEach(tag => {
            element.tagIn += `<span class="tag>${tag}</span>`;
            link.classList.add(tag.replace(/\s/g, "_").toLowerCase());
            // console.log(`${tag.toLowerCase().replace("/\s/g", "-")}`);
          })
        } else {
          element.tagIn = " ";
        }
        if (element.language) {
          const langList = element.language.split(", ");
          element.langIn = " ";
          langList.forEach(lang => {
            element.langIn += ` <span class="lang">${lang}</span>`
          });
        } else {
          element.langIn = " ";
        }
        const par = document.createElement("p");
        par.classList.add("hidden-stuff");
        par.innerHTML += `<span class="lang">${element.langIn}</span> <span class="tags">${element.tagIn}</span>`
        par.innerHTML += `<span>${element.description} ${element.participant}</span>`
        par.innerHTML += `<span>${element.tent}</span>`

        // item.innerHTML += ;
        // item.innerHTML += `<p class="meta"><span class="source-date">sourceDate: ${element.date} ${element.time}</span> —  <span class="local">local: ${dateLocal}</span>`;
        if (moment().diff(dateEvent.add(1, 'hours')) > 0) {
          link.classList.add('passed')
        };
        link.insertAdjacentElement("beforeend", item);
        link.insertAdjacentElement("beforeend", par);
        dayList.insertAdjacentElement("beforeend", link);
      }

      orderDay(dayList);

    }



    if (dayCount == "17" || dayCount == "31") {
      if (dayList.childNodes.length) {
        day.classList.add(dayNumber.locale("en").format("dddd"));

        day.insertAdjacentHTML("afterbegin", `<h3><span class="day-letter">${dayNumber.locale(getNavigatorLanguage()).format("dddd")}</span> <span class="number">${dayCount}</span></h3>`);
        day.insertAdjacentElement("beforeend", dayList);
        container.insertAdjacentElement("beforeend", day);
      }
    } else {
      day.classList.add(dayNumber.locale("en").format("dddd"));
      day.insertAdjacentElement("beforeend", dayList);

      day.insertAdjacentHTML("afterbegin", `<h3><span class="day-letter">${dayNumber.locale(getNavigatorLanguage()).format("dddd")}</span> <span class="number">${dayCount}</span></h3>`);      // day.insertAdjacentHTML("afterbegin", `<h3>${dayCount}</h3>`);

      container.insertAdjacentElement("beforeend", day);
    }
  }

}


// 
function updateDate() {

  const dates = document.querySelectorAll(".event");
  const listDates = document.querySelectorAll(".an-event");

  targetDate = selector.value;

  dates.forEach((event) => {
    const sourceDate = moment(event.dataset.sourceDate);
    const upDate = sourceDate.tz(targetDate).clone().format("LLLL z");
    event.querySelector(".local").innerHTML = upDate;
  });

  listDates.forEach(event => {
    const sourceDateList = moment(event.dataset.sourceDate);
    const update = sourceDate.tz(targetDate).clone().format("LLLL z");
    event.querySelector(".date").innerHTML = sourceDate.tz(targetDate).clone().format("LLLL z");
  })
};






function updateCalendar() {
  targetDate = selector.value || localDate;
  createCalendar(sortedDates, targetDate);
  createList(sortedDates, targetDate);
}


function orderByDate(dates) {
  // return sortedDates.sort((a, b) => new moment(a.date).diff(b.date));
  //   sortedDates = _.sortBy(sortedDates, function (o) {
  //     return (o.fullTime);
  //   });
  // }
  // console.log(dates.sort(
  //   (a, b) => new moment(a.fullTime.format('YYYYMMDD-HH:mm')).diff(b.fullTime.format('YYYYMMDD-HH:mm')) 
  //   ));
  const sortedDates =

    dates.sort(
      (a, b) => new moment(a.fullTime).diff(b.fullTime)
    );
  return sortedDates;
};


function randomColorCal() {
  color = [
    "--sunny",
    "--spades",
    "--waters",
    "--forest",
    "--mountains",
    "--symbols",
  ];
  let num = randomShapeNumber(5);
  return color[num];
}

function randomShapeNumber(max) {
  return Math.floor(Math.random() * max);
}

function openContent(event) {
  event.classList.toggle("hide");
}


async function createList(sortedDates, targetDate) {
  containerList.innerHTML = `<button id="close" onclick="closeList()">×</button>`;

  for (let i = 0; i < sortedDates.length; i++) {
    let el = sortedDates[i];
    let timeClean = `${el.date}T${el.time}.000`;
    const p = document.createElement("div");
    p.classList.add("an-event");
    p.classList.add("hide");
    let color, tentName;
    switch (el.tent) {
      case 'demo':
        color = "sunny";
        hash="demo"
        tentName = 'Demo & How To'
        break;
      case 'performance':
        color = "symbols"
        hash='performance'
        tentName = 'Performance'
        break;
      case 'firesideChats':
        color = "waters"
        tentName = 'Fireside Chats'
        hash = 'fireside-chats'
        break;
      case 'discussion':
        color = "forest"
        tentName = 'Discussion'
        hash = 'discussion'
        break;
      case 'micropublications':
        color = "spades"
        tentName = 'Micropublications'
        hash = 'micropubs'
        break;
      case 'longEvent':
        color = "mountains"
        tentName = 'work of Bob Stein'
        hash = 'micropubs'

        break;
      default:
        color = "black"
        tentName = 'this event hasn’t been curated yet'
        break
    }
    // let tent = createTent("red", "spades");
    let dateEvent;
    if (el.timezone) {
      dateEvent = moment
        .tz(timeClean, el.timezone)
        .clone()
        .tz(targetDate);
    }
    else {
      console.log(`${el.name} has no timezone`)
    }



    p.setAttribute("style", `--color-tent: var(--${color})`)

    const tent = createTent(`var(--${color}`, color);
    // p.innerHTML += ``



    // get all images
    const pics = document.createElement("figure");
    if (el.images.length == 0) {
      const illus = logo;
      pics.innerHTML = illus;
    }
    el.images.forEach((img) => {
      let illus = document.createElement("img");
      if (img.ext != ".bin") {
        illus.src = `events/images/${img.hash}${img.ext}`
        pics.insertAdjacentElement("beforeend", illus);

      } else {
        illus = logo;
        pics.innerHTML = illus;
      }



    });

    p.insertAdjacentElement("beforeend", pics);
    pics.insertAdjacentHTML("beforebegin", ` <p class="time"><span class="month">${dateEvent.clone().tz(targetDate).format("MMMM")}</span><span class="dayNum">${dateEvent.clone().tz(targetDate).format("DD")}</span>  <span class="hour">${dateEvent.clone().tz(targetDate).format("LT")}</span><span class="tz">${dateEvent.clone().tz(targetDate).format("z")}</span></p>`)
    // const icsLink = document.createElement('a');
    // icsLink.href = `events/ics/event-${el.id}.ics`;
    // icsLink.innerHTML = 'ADD TO YOUR CALENDAR'
    // pics.insertAdjacentElement("beforeend", icsLink);
    if (el.tags) {
      const tagList = el.tags.split(", ");
      el.tagIn = "";

      tagList.forEach(tag => {
        el.tagIn += `<span class="tag">${tag}</span>`
      })
    } else {
      tagIn = " ";
    }
    if (el.language) {
      const langList = el.language.split(", ");
      el.langIn = " ";
      langList.forEach(lang => {
        el.langIn += ` <span class="lang">${lang}</span>`
      });
    } else {
      el.langIn = " ";
    }

    let participant = (el.participant && el.participant != el.username) ? `<p class="participant">Participants: ${el.participant}</p>` : " ";

    p.id = `event-${el.id}`;

    p.innerHTML += `

    <div class="event-content">
    <div class="event-meta"> 
    <p  class="dl-ics"><a href="events/ics/event-${el.id}.ics">Add to your calendar</a><p>
    <p class="tent"><span class="event-tent">${tent}</span><span class="tent-name">${tentName}</span></p>
    <span class="lang">${el.langIn}</span>
    
    ${(el.tagIn) ? '<p class="tags">' + el.tagIn + '</tags>' : ' '}
    

    </div>

        <div class="event-text">


        <h4>${el.title}</h4>
        ${ el.registration === true ? '<p class="registration-needed"><span>This event requires registration.</span> ' + registration + '</p>' : ""}

        ${participant}
        <div class="description">${md.render(el.description)}</div>
        
        <p class="link"><a target="_blank" href="${el.url || '#'}">${el.url || 'link to be announced'}</a></p>

        <p class="from">Organized by ${el.username}</p>

        ${(el.ShareUserName === true) ? '<p class="mail">Contact:' + el.usermail + '</p>' : " "}
        ${(el.socialAccount === true) ? '<p class="mail">Contact:' + el.socialAccount + '</p>' : " "}



        </div>
    </div>
    </div>`

    document.querySelector("main #list").insertAdjacentElement("beforeend", p);
  }

}




// selector.addEventListener("onchange", updateDate(selector.value));


function orderDay(list) {

  var items = list.childNodes;
  var itemsArr = [];
  for (var i in items) {
    if (items[i].nodeType == 1) { // get rid of the whitespace text nodes
      itemsArr.push(items[i]);
    }
  }

  itemsArr.sort(function (a, b) {
    return a.dataset.sourceTime == b.dataset.sourceTime
      ? 0
      : (a.dataset.sourceTime > b.dataset.sourceTime ? 1 : -1);
  });

  for (i = 0; i < itemsArr.length; ++i) {
    list.appendChild(itemsArr[i]);
  }
}



// behaviors and UI

// document.querySelectorAll(".event").addEventListener("click", console.log("lol"))


// const calEvents = document.querySelectorAll(".event");

// calEvents.forEach(ev => {
//   ev.addEventListener('click', showEvent(ev));
// });



// function showEvent(event) {
//   // const item = document.querySelector(event.getAttribute("href"));
//   // console.log('item', item)
//   document.querySelector(event.hash).style.display = "block";
//   console.log(event.hash);
//   document.querySelector("#list").style.display = "grid";
// }


// // 


// function callback(e) {
//   var e = window.e || e;

//   if (e.target.tagName !== 'A')
//       return;
//       console.log("click link")
//   // Do something
// }

// if (document.addEventListener)
//   document.addEventListener('click', callback, false);
// else
//   document.attachEvent('onclick', callback);

function showEvent(event) {
  event.preventDefault;
  document.querySelector("#hideList").classList.remove("hide");
  document.querySelector(event.hash).classList.remove("hide");
  document.querySelector("#list").setAttribute("style", document.querySelector(event.hash).getAttribute("style"));
  document.querySelector("#list").classList.remove("hide");

}

function closeList() {
  // this.preventDefault;
  document.querySelector("#list").classList.add("hide");
  document.querySelectorAll(".an-event").forEach(e => { e.classList.add("hide") });
  document.querySelector("#hideList").classList.add("hide");

}




// hide on click outside


function listAllTent() {
  const tentList = document.querySelector('#tents');
  tentList.innerHTML = "";
  const color = ["sunny", "symbols", "waters", "forest", "spades", "mountains", 'black', 'grey']
  const hash = ["demo", "performance", "fireside-chats", "discussion", "micropubs", "mountains", 'black', 'grey']
  const type = ['Demo & How To', 'Performance', 'Fireside Chats', 'Discussion', 'Micropublications', 'The work of Bob Stein', 'not Accepted yet', 'Show all']
  for (i = 0; i < 8; i++) {
    const tent = document.createElement("li");
    tent.id = hash[i];
    if (tent.id == 'grey') {
      tent.setAttribute("onclick", `showAllEvents()`)
    } else {
      tent.setAttribute("onclick", `showOnlyEvents('${hash[i]}')`)
    }
    tent.setAttribute("style", `--color-tent: var(--${color[i]})`)
    tent.innerHTML += createTent(color[i], color[i]);
    tent.innerHTML += `<p>${type[i]}</p>`
    tentList.insertAdjacentElement("afterbegin", tent);
  }

}

function toggleEvent(event) {
  document.querySelectorAll(`.${event}`).forEach(e => {
    e.classList.toggle("hide");
  })

}

function showAllEvents() {
  document.querySelectorAll(`.event`).forEach(e => {
    e.classList.remove("hide");
  })
  document.querySelectorAll(`#tents li`).forEach(t => {
    t.style.opacity = "1";
  });
}



async function showOnlyEvents(event) {
  if (showAll == false && event == showEventToggle) {
    showAllEvents();
    showAll = true;
    showEventToggle = "";
  } else {
    document.querySelectorAll(`.event`).forEach(e => {
      if (e.classList.contains(`tent-${event}`)) {
        e.classList.remove("hide");
      } else {
        e.classList.add("hide");
      }

      document.querySelectorAll(`#tents li`).forEach(t => {
        if (t.id == event) {
          t.style.opacity = '1'; 
        } else {
          t.style.opacity = ".3";
        }
      });

      document.querySelectorAll(`.${event}`).forEach(e => {
        e.classList.remove("hide");
      });
      showEventToggle = event;
      showAll = false;
    });
  }
}

function hasLoaded() {
  setTimeout(
    function () {
      document.querySelector("#loader").classList.add("remove");
    }, 100);
  setTimeout(
    function () {
      document.querySelector("#loader").remove();
    }, 1000);
  selector.removeAttribute("disabled");
}

// function willLoad() {
//   document.querySelectorAll("main > *").forEach(el => { el.style.opacity = "0" });
// }


async function openURL() {
  showAllEvents();
  if (window.location.hash) {
    if (RegExp('event-').test(window.location.hash)) {
      showEvent(location);
    } else {
      showOnlyEvents(window.location.hash.replace(/^#/, ''));
      // document.querySelector('#search').placeholder = window.location.hash.replace(/^#/, '').replace(/_/, ' ');
    }

  }
}

document.addEventListener("keydown", function (event) {
  if (event.key === "Escape") {
    document.querySelector("#hideList").classList.add("hide");
    document.querySelector("#list").classList.add("hide");
    document.querySelectorAll('#list .an-event').forEach(e => e.classList.add("hide"));
  }
})








// async function showSameLanguage(this) {
//   // if(event.language.contains('')) {}
//   // if (language contains("french"));
// }

// add search to calendar ?/

// filter by tag, language, etc.



function searchAnEvent() {
  // Declare variables
  // showAllEvents();

  const input = document.querySelector('#search');
  let filter = input.value.toUpperCase();
  const ul = document.querySelector("#myData");
  const li = ul.querySelectorAll('a');
  // Loop through all list items, and hide those who don't match the search query
  li.forEach(event => {
    txtValue = event.textContent.toUpperCase() || event.innerText.toUpperCase();
    if (txtValue.indexOf(filter) > -1) {
      event.style.display = "";
    } else {
      event.style.display = "none";
    }
  })
}






