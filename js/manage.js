const server = 'https://openpublishingfest2.cloud68.co';
let token;

let population = document.querySelector("main");

function fetch() {
    username = document.querySelector("#username").value;
    password = document.querySelector("#password").value;
    login(username, password)
}

async function login(username, password) {
    axios
        .post(`${server}/auth/local`, {
            identifier: username,
            password: password,
        })
        .then(function (response) {

            token = response.data.jwt;
            // document.querySelector(".form").remove();
            if (response) {
                loadAttendees();
                document.querySelector(".form").remove();
                
            }
        })
        .catch(function (error) {
            console.log('there is an error:', error)
            document.querySelector(`.response`).textContent = "sorry mate can’t connect, forgot you pass again?";
        })
        .finally(function () { });
}



async function loadAttendees() {
    axios
        .get(`${server}/participants?_limit=-1`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        .then(function (response) {
            const data = response.data;
            // console.log(response);
            showParticipants(data)
            // backupDBAttendees(`attendees/data.js`, data);
            // backupIMGAttendees(data);

            // console.log('response', response)
        })
        .catch(function (e) {
            console.log('e:', e);
        })
}




async function acceptOne(id, el) {

    axios
        .put(`${server}/participants/${id}`, { "accept": true }, {
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
        .then(function (response) {
            console.log("ok");
            console.log('response', response)
            el.closest(".p").remove();


            // document.querySelector("section.updat").innerHTML = '<p class="thanks">Welcome to the band! We’ll add your name to the list.</p>';
        })
        .catch(error => {
            console.log(error);
        });

}





async function refuseOne(id, el) {

    axios
        .put(`${server}/participants/${id}`, { "accept": false }, {
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
        .then(function (response) {
            console.log("ok");
            console.log('response', response)
            el.closest(".p").remove();

            // document.querySelector("section.updat").innerHTML = '<p class="thanks">Welcome to the band! We’ll add your name to the list.</p>';
        })
        .catch(error => {
            console.log(error);
        });

}


async function showParticipants(response) {
    const list = document.createElement("ul");
    response.forEach(participant => {
        if (participant.picture != null) {
            if (participant.picture.ext != ".bin") {
                if (participant.accept === null) {
                    const attendee = document.createElement("li");
                    attendee.classList.add('p');

                    // name
                    const name = document.createElement("h2");
                    name.classList.add('name');
                    name.textContent = participant.name;

                    // mail
                    const mail = document.createElement("a");
                    mail.setAttribute("href", `mailto:${participant.mail}`);
                    mail.setAttribute("class", "mail")
                    mail.innerHTML = participant.mail;

                    const text = document.createElement("div");
                    text.classList.add("content");
                    // image

                    const pic = document.createElement("figure");
                    let illus = document.createElement("img");
                    illus.src = server + participant.picture.url;
                    pic.insertAdjacentElement("beforeend", illus);

                    const ok = document.createElement("button");
                    ok.setAttribute("onclick", `acceptOne(${participant.id}, this)`);
                    ok.classList.add("ok")
                    ok.innerHTML = "accept";
                    const nope = document.createElement("button")
                    nope.setAttribute("onclick", `refuseOne(${participant.id}, this)`);
                    nope.classList.add("nope")
                    nope.innerHTML = "refuse";


                    attendee.insertAdjacentElement("beforeend", pic);
                    text.insertAdjacentElement("beforeend", name);
                    text.insertAdjacentElement("beforeend", mail)

                    attendee.insertAdjacentElement("beforeend", text)
                    attendee.insertAdjacentElement("beforeend", ok)
                    attendee.insertAdjacentElement("beforeend", nope)
                    // text.insertAdjacentElement("beforeend", bio)

                    list.insertAdjacentElement("beforeend", attendee);
                }
            }
        }
    })
    population.insertAdjacentElement("beforeend", list);
}





// search
