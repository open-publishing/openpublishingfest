function randomShapeNumber(max) { return Math.floor(Math.random() * max) };

let showTop = randomShapeNumber(5);
let showBottom = randomShapeNumber(5);

if (showBottom == showTop) { showBottom = randomShapeNumber(6) };

if (document.querySelectorAll("svg #top g").length != "") {
    document.querySelectorAll("svg #top g").forEach((c, index) => {
        c.style.display = "none";
        if (index == showTop) {
            c.style.display = "block";
        }
    })

    document.querySelectorAll("svg #bottom g").forEach((c, index) => {
        c.style.display = "none";
        if (index == showTop) {
            c.style.display = "block";
        }
    })
    color = ["--sunny", "--spades", "--waters", "--forest", "--mountains", "--symbols"];
    let num = randomShapeNumber(5);
    // document.querySelector(".splash .text svg #Text").setAttribute("fill", `var(${color[num]})`);
    document.querySelector(".splash nav").setAttribute("style", `--titleColor: var(${color[num]});`);
}

// calendar script

// get browser lang
const getNavigatorLanguage = () => {
    if (navigator.languages && navigator.languages.length) {
        return navigator.languages[0];
    } else {
        return navigator.userLanguage || navigator.language || navigator.browserLanguage || 'en';
    }
};



//   create list of event

// let eventShown = "";

// window.onload = () => {
//     // find calendar
//     let elementCalendar = document.querySelector("#calendar");

//     // setup calendar
//     let calendar = new FullCalendar.Calendar(elementCalendar, {
//         plugins: ['dayGrid', 'timeGrid', 'list', 'momentTimezone'],
//         defaultView: 'dayGridMonth',
//         locale: getNavigatorLanguage(),

//         header: {
//             left: "prev next today",
//             center: " title ",
//             right: "dayGridMonth, timeGridDay",
//         },
//         buttonText: {
//             month: "Festival",
//             day: "Today"
//         },
//         eventClick: function (e) {
//             eventShown = document.querySelector(`#${e.event.id}`);
//             eventShown.classList.toggle('hide');
//             document.querySelector("main").classList.toggle('hide');
//         },
//         events
//     }
//     );

//     calendar.render();
// }

// console.log(eventShown);

// document.querySelector("#close").addEventListener("click", (e) => {
//     eventShown.classList.add("hide");
//     e.target.closest("main").classList.add("hide");
//     eventShown = "";
// });