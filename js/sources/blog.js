const axios = require('axios').default;
const moment = require('moment-timezone');
const MarkdownIt = require('markdown-it');
const implicitFigures = require('markdown-it-implicit-figures');
const blockEmbedPlugin = require('markdown-it-block-embed');

const getNavigatorLanguage = () =>
  navigator.languages && navigator.languages.length
    ? navigator.languages[0]
    : navigator.userLanguage ||
    navigator.language ||
    navigator.browserLanguage ||
    "en";

const server = `https://openpublishingfest2.cloud68.co`;

md = new MarkdownIt();
md.use(implicitFigures, {
    dataType: false,  // <figure data-type="image">, default: false
    figcaption: false,  // <figcaption>alternative text</figcaption>, default: false
    tabindex: true, // <figure tabindex="1+n">..., default: false
    link: true // <a href="img.png"><img src="img.png"></a>, default: false
  }).use(adjustHeadingLevel, { firstLevel: 2 });
  md.use(blockEmbedPlugin, {
    containerClassName: "video-embed"
  });
  
load();
openURL();

async function openURL() {
  if (window.location.hash) {
    console.log(location.hash)
    if(document.querySelector(`${location.hash}`) != null) {
      document.querySelector(`${location.hash}`).scrollTo();
    }
  }
}


async function load() {
    axios
        .get(`${server}/articles?_sort=created_at:DESC`, {
        })
        .then(function (response) {
            let data = response.data;
            // data = orderByDate(data);
            orderArticles(data);
          
        });
}

const container = document.querySelector("main");

function orderArticles(data) {

    data.forEach((article) => {
        if (article.publish === true) {
            const section = document.createElement("section");
            section.classList.add("article");
            section.id = `art-${moment(article.created_at).format("YYYY-MM-DD-hh-mm")}`;
            const but = document.createElement(`button`);
            but.classList.add('copypaste');
            but.setAttribute(`data-clipboard-text`, `https://www.openpublishingfest.org/blog.html#art-${moment(article.created_at).format("YYYY-MM-DD-hh-mm")}`)
            but.innerText = `copy link`;
            section.innerHTML = `On <span class="blog-date">${moment(article.created_at).locale(getNavigatorLanguage()).format("ll")}</span>`
            section.innerHTML += but.outerHTML;
            section.innerHTML += (article.author) ? `by <span class="author">${article.author}</span>` : "";
            section.innerHTML += `<h1>${article.title}</h1>`;
            section.innerHTML += md.render(article.content);
            container.insertAdjacentElement("beforeend", section);
            
        }
    });
    if(window.location.hash) {
    window.location.href = document.location.href;
  }

}

// change heading levels


function getHeadingLevel(tagName) {
    if (tagName[0].toLowerCase() === 'h') {
      tagName = tagName.slice(1);
    }
    
    return parseInt(tagName, 10);
  }
  
  function adjustHeadingLevel(md, options) {
    var firstLevel = options.firstLevel;
  
    if (typeof firstLevel === 'string') {
      firstLevel = getHeadingLevel(firstLevel);
    }
  
    if (!firstLevel || isNaN(firstLevel)) {
      return;
    }
  
    var levelOffset = firstLevel - 1;
    if (levelOffset < 1 || levelOffset > 6) {
      return;
    }
  
    md.core.ruler.push("adjust-heading-levels", function(state) {
      var tokens = state.tokens
      for (var i = 0; i < tokens.length; i++) {
        if (tokens[i].type !== "heading_close") {
          continue
        }
  
        var headingOpen = tokens[i - 2];
        // var heading_content = tokens[i - 1];
        var headingClose = tokens[i];
  
        // we could go deeper with <div role="heading" aria-level="7">
        // see http://w3c.github.io/aria/aria/aria.html#aria-level
        // but clamping to a depth of 6 should suffice for now
        var currentLevel = getHeadingLevel(headingOpen.tag);
        var tagName = 'h' + Math.min(currentLevel + levelOffset, 6);
  
        headingOpen.tag = tagName;
        headingClose.tag = tagName;
      }
    });
  }

  // function getHash(el) {
  //   const hash = el.closest(".article");
  //   console.log(hash);
  //     window.clipboardData.setData("Text", `https://www.openpublishingfest.org/#${hash}`);
  // }


