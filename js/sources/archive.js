const moment = require('moment-timezone');
const MarkdownIt = require('markdown-it');
const implicitFigures = require('markdown-it-implicit-figures');
const blockEmbedPlugin = require('markdown-it-block-embed');

const dayStart = 17, dayEnd = 30;

const container = document.querySelector("#archives")

let localDate = moment.tz.guess();

let targetDate = localDate;


md = new MarkdownIt();
md.use(implicitFigures, {
    dataType: false,  // <figure data-type="image">, default: false
    figcaption: false,  // <figcaption>alternative text</figcaption>, default: false
    tabindex: true, // <figure tabindex="1+n">..., default: false
    link: true // <a href="img.png"><img src="img.png"></a>, default: false
}).use(adjustHeadingLevel, { firstLevel: 2 }).use(require('markdown-it-iframe'))
md.use(blockEmbedPlugin, {
    containerClassName: "video-embed",
    outputPlayerSize: false
}).use(require('markdown-it-html5-embed'), {
    html5embed: {
      useImageSyntax: true, // Enables video/audio embed with ![]() syntax (default)
      useLinkSyntax: true   // Enables video/audio embed with []() syntax
  }});


const getNavigatorLanguage = () =>
    navigator.languages && navigator.languages.length
        ? navigator.languages[0]
        : navigator.userLanguage ||
        navigator.language ||
        navigator.browserLanguage ||
        "en";

loadNoDb();



moment.locale(getNavigatorLanguage());




async function loadNoDb() {

    const data = allEventsData;

    data.forEach(el => {
        el.fullTime = moment(`${el.date}T${el.time}`).tz(el.timezone).tz(targetDate);
    });
    // willLoad();
    sortedDates = orderByDate(data);
    createArchives(sortedDates, targetDate);
}



async function createArchives(sortedDates, targetDate) {
    // for (let dayCount = dayStart; dayCount < dayEnd + 1; dayCount++) {
    //     section = document.createElement("section");
    //     section.classList.add("day");

    for (let i = 0; i < sortedDates.length; i++) {
        let el = sortedDates[i];

        if (moment().diff(el.fullTime.add(1, 'hours')) > 0) {
            if (el.archiveLink != null && el.archiveLink != 'nope' && el.accept === true ) {
                console.log('el.archiveLink', el.archiveLink)
                const event = document.createElement('li');
                let color, hash, tentName;

                switch (el.tent) {
                    case 'demo':
                        color = "sunny";
                        hash = "demo"
                        el.tentName = 'Demo & How To'
                        break;
                    case 'performance':
                        color = "symbols"
                        hash = 'performance'
                        el.tentName = 'Performance'
                        break;
                    case 'firesideChats':
                        color = "waters"
                        el.tentName = 'Fireside Chats'
                        hash = 'fireside-chats'
                        break;
                    case 'discussion':
                        color = "forest"
                        el.tentName = 'Discussion'
                        hash = 'discussion'
                        break;
                    case 'micropublications':
                        color = "spades"
                        el.tentName = 'Micropublications'
                        hash = 'micropubs'
                        break;
                    case 'longEvent':
                        color = "mountains"
                        el.tentName = 'All along the festival'
                        break;
                    default:
                        color = "black"
                        el.tentName = 'this event hasn’t been curated yet'
                        break
                }

                event.setAttribute('style', `--color-tent: var(--${color})`)
                event.setAttribute('onclick', `showEl(this)`)
                event.classList.add('archive-event');
                const div = document.createElement('div');
                div.classList.add("content");
                div.id = `archive-event-${el.id}`
                div.innerHTML += ``
                div.innerHTML += `<p class="meta"><span class="date">${moment(el.date).clone().format("LL")}</span> in <span class="tent">${el.tentName}</span></p>`
                div.innerHTML += `<h2>${el.title}</h2>`;

                div.innerHTML += (el.participant) ? `<p class="participants">${el.participant}</p>` : ``;

                div.innerHTML += `<p class="description">${el.description}</p>`;
                event.insertAdjacentElement('beforeend', div);
                event.innerHTML += md.render(`${el.archiveLink}`)
                container.insertAdjacentElement('beforeend', event);
            }
        }
    }
    // section.insertAdjacentElement('beforeend', event);

}
// }
function showEl(el) {
    el.classList.toggle('show');
}


function orderByDate(dates) {
    const sortedDates = dates.sort((a, b) => new moment(a.fullTime).diff(b.fullTime));
    return sortedDates;
};



async function openURL() {
    if (window.location.hash) {
      if (RegExp('archive-event-').test(window.location.hash)) {
        document.querySelector(location.hash).scrollTo();

      }
    }
  }
// change heading levels


function getHeadingLevel(tagName) {
    if (tagName[0].toLowerCase() === 'h') {
        tagName = tagName.slice(1);
    }

    return parseInt(tagName, 10);
}

function adjustHeadingLevel(md, options) {
    var firstLevel = options.firstLevel;

    if (typeof firstLevel === 'string') {
        firstLevel = getHeadingLevel(firstLevel);
    }

    if (!firstLevel || isNaN(firstLevel)) {
        return;
    }

    var levelOffset = firstLevel - 1;
    if (levelOffset < 1 || levelOffset > 6) {
        return;
    }

    md.core.ruler.push("adjust-heading-levels", function (state) {
        var tokens = state.tokens
        for (var i = 0; i < tokens.length; i++) {
            if (tokens[i].type !== "heading_close") {
                continue
            }

            var headingOpen = tokens[i - 2];
            // var heading_content = tokens[i - 1];
            var headingClose = tokens[i];

            // we could go deeper with <div role="heading" aria-level="7">
            // see http://w3c.github.io/aria/aria/aria.html#aria-level
            // but clamping to a depth of 6 should suffice for now
            var currentLevel = getHeadingLevel(headingOpen.tag);
            var tagName = 'h' + Math.min(currentLevel + levelOffset, 6);

            headingOpen.tag = tagName;
            headingClose.tag = tagName;
        }
    });
}



function searchArchive() {
    // Declare variables
    const input = document.querySelector('#search');
    let filter = input.value;
    const ul = document.querySelector("#participants");
    const li = ul.querySelectorAll('li');
    console.log(filter);
    // Loop through all list items, and hide those who don't match the search query
        li.forEach( h2 => {
        txtValue = h2.textContent.toLowerCase() || h2.innerText.toLowerCase();
        if (txtValue.indexOf(filter) > -1) {
           h2.style.display = "";
        } else {
            h2.style.display = "none";
        }
    })
}