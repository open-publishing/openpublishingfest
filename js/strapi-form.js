// setup standard localtime and time zones for the form
const localDate = Intl.DateTimeFormat().resolvedOptions().timeZone
const selector = document.querySelector('#timezoneSelect')

selector.querySelectorAll('option').forEach(zn => {
    if (zn.value === localDate) { zn.setAttribute('selected', 'selected') }
})

// set up form
const formElement = document.querySelector('#form');



formElement.addEventListener("submit", e => {


    e.preventDefault();


    const formElements = formElement.elements;

    if (formElements[19].value == "") {
        const formContent = new FormData(formElement);

        const data = {};


        for (let i = 0; i < formElements.length; i++) {

            const currentElement = formElements[i];
            
            if (!['submit', 'file'].includes(currentElement.type) && currentElement.name != "enrico") {
                // formContent.append(currentElement.name, currentElement.value)
                if (currentElement.name == "time" && currentElement.value.indexOf(':00.000') <= -1) {
                    currentElement.value = currentElement.value + ':00.000'
                }

                if(currentElement.type == "radio" && currentElement.checked) {
                    currentElement.value = currentElement.value.replace(`"`, ``);
                    currentElement.value = new Boolean(currentElement.value);
                }
        // formContent.append("shareUserName",)
        

                data[currentElement.name] = currentElement.value;
            } else if (currentElement.type === 'file') {
                for (let i = 0; i < currentElement.files.length; i++) {
                    const file = currentElement.files[i];
                    // console.log("file", file)    
                    // formContent.append(currentElement.name, file, File.name);
                    // data[currentElement.name] = currentElement.value;

                }
            }

        }
        const stringdata = data;
        formContent.append("data", JSON.stringify(stringdata)); 

        console.log( JSON.stringify(formContent));
        // console.log(JSON.stringify(data))
        // formContent.append('data', JSON.stringify( data));
        // console.log('formContent.files.images', formContent.files.images);

        // formContent.append(`files.${attributeName}`, new File())
        formContent.delete("enrico");

        console.log(...formContent);

        axios
            .post(`https://openpublishingfest2.cloud68.co/events/`, formContent, {
                onUploadProgress: event => {
                    console.log(event.loaded, event.total);
                    document.querySelector("section.update").innerHTML = `<p class="event-loading">${Math.floor(event.loaded/event.total * 100)}%</p>`
                    if (event.loaded == event.total) {
                        console.log(event);
                        document.querySelector("section.update").innerHTML = "Please wait while we’re uploading your submission to our servers."
                    }
                }                
            })
            .then(function (response) {
                console.log(response);
                document.querySelector("section.update").innerHTML = '<p class="thanks">Thanks for your proposal, we’ll contact you soon!</p>';    
            })
            .catch(error => {
                console.log(error);

                document.querySelector("section.update").innerHTML =`
                <p class="thanks">An error occured:  ${error}</p>
                <p class="thanks">There has been an error while uploading your form, please contact <a href="mailto:julien@coko.foundation">julien @ coko . foundation</a>. or <a href="form.html">try again</a>.</p>`;   
                scrollTo(document.querySelector("section.update")); 
            });

    }

});

function sizeOfFile() {

    var filesize = document.querySelector('input[type="file"]').files[0].size;
    if (filesize > 2000000) {
    this.value = "";    
    alert("images must be 2mb maximum");
    }
    
    console.log(filesize);
}

// function mailValidation() {

//     var mail = document.querySelector('input[type="email"]')
//     if (emailIsValid(mail.value) {
//     ;
//     });
// }

function emailIsValid (email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  }



  const emailZone = document.querySelector('input[type="email"]');
//   document.querySelector('input[type="email"]').addEventListener('change', emailIsValid(document.querySelector('input[type="email"]').value));

emailZone.addEventListener("change", console.log('change'));




// async function load() {
//     axios
//       .get(`${server}/events`, {
//         headers: {
//           Authorization: `Bearer ${window.sessionStorage.accessToken}`,
//         },
//       })
//       .then(function (response) {
  
//         listAllTent();
//         const data = response.data;
//         data.forEach(el => {
//           el.fullTime = moment(`${el.date}T${el.time}`).tz(el.timezone).tz(targetDate);
//         });
//         // willLoad();
//         sortedDates = orderByDate(data);
//         createCalendar(sortedDates, targetDate);
//         createList(sortedDates, targetDate);
//         hasLoaded();
//         openURL();
//       })
//       .catch( function (e) {
//           console.log('e:',e);
//           loadNoDb();
        
//       })
//   }
  